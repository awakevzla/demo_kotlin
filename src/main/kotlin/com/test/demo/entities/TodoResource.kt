package com.test.demo.entities

import org.springframework.data.jpa.repository.JpaRepository
import java.time.Instant
import javax.persistence.*

interface TodoRepository: JpaRepository<Todo,Long>

@Entity
class Todo(@Id @GeneratedValue(strategy = GenerationType.IDENTITY) val Id:Long = 0, var name: String = "", var description: String = "", var actived: Boolean = true, val created_at: Instant = Instant.now())