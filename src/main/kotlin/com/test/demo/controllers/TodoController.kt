package com.test.demo.controllers

import com.test.demo.entities.Todo
import com.test.demo.entities.TodoRepository
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.config.annotation.EnableWebMvc

@RestController @RequestMapping(value = ["/todo"]) @EnableWebMvc
class TodoResource(val todoRepo: TodoRepository){
    @GetMapping(value = ["/"])
    fun getAll() = todoRepo.findAll()

    @GetMapping(value = ["/{id}"])
    fun getOne(@PathVariable id: Long) = todoRepo.findById(id)

    @PostMapping(value = ["/"])
    fun create(@RequestBody document: Todo) = todoRepo.save(Todo(name = document.name,description = document.description))

    @DeleteMapping(value = ["/{id}"])
    fun delete(@PathVariable id: Long) = todoRepo.deleteById(id)

    @PutMapping(value = ["/{id}"])
    fun update(@PathVariable id: Long, @RequestBody document: Todo): Todo{
        val toUpdate:Todo = todoRepo.findById(id).orElseThrow{Exception("Server error")}
        toUpdate.name = document.name
        toUpdate.description = document.description
        return todoRepo.save(toUpdate)
    }
}
